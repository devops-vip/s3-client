import React, { useState } from "react";
import axios from "axios";

function App() {
  const [fileData, setFileData] = useState();

  const fileChangeHandler = (e) => {
    setFileData(e.target.files[0]);
  };

  const onSubmitHandler = (e) => {
    console.log("file data ====>", fileData);
    const data = new FormData();
    data.append("image", fileData, "image"); // image key to use in Postman
    const server = process.env.REACT_APP_IP;
    // Send reqest to backend - Single upload
    axios
      .post(`${server}/single`, data)
      .then((result) => {
        console.log("File sent to S3 successfully", result);
      })
      .catch((err) => {
        console.log("Something Went Wrong", err);
      });
  };

  return (
    <div className="App">
      <h1>Uploading Files using Multer</h1>
      <input type="file" onChange={fileChangeHandler} />
      <br />
      <br />
      <button onClick={onSubmitHandler}>Send to backend</button>
      <br />
      {/* <img
        src="http://localhost:5000/images/1664979801754-Screenshot from 2022-09-22 15-10-07.png"
        alt="S3 file"
        width="800"
      /> */}
    </div>
  );
}

export default App;
